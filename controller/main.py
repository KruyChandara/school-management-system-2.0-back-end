# -*- coding: utf-8 -*-
from openerp import http
from openerp.http import request
from openerp.exceptions import except_orm
from datetime import date, timedelta
from time import strptime
import datetime
import json
from openerp.http import Response
from dateutil.relativedelta import relativedelta
import base64
from openerp.addons.sms2.models.response import valid_response, invalid_response


def assign(obj, keyPath, value):
    for key in keyPath:
        if key == keyPath[-1]:
            obj[keyPath[-1]] = value
            break
        if not key in obj:
            obj[key] = {}
        obj = obj[key]


day = {'Sunday': 1, 'Monday': 2, 'Tuesday': 3,
       'Wednesday': 4, 'Thursday': 5, 'Friday': 6, 'Saturday': 7}
day_array = ['Sunday', 'Monday', 'Tuesday',
             'Wednesday', 'Thursday', 'Friday', 'Saturday']


class MainController(http.Controller):

    @http.route('/sms/<route>', type='http', auth='user', website=True, cors='*')
    def render_page(self, **kw):
        return http.request.render('sms2.sms2')

    @http.route('/sms/<route>/<s>', type='http', auth='user', website=True, cors='*')
    def render_page2(self, **kw):
        return http.request.render('sms2.sms2')

    @http.route('/create-attendance-sheet', type='json', auth='public', cors='*', csrf=False)
    def create_attendance_sheet(self, **kw):
        op_attendance = request.env['op.attendance.sheet'].sudo()
        op_student = request.env['op.student'].sudo()
        op_subject = request.env['op.subject'].sudo()
        op_period = request.env['op.period'].sudo()
        op_batch = request.env['op.batch'].sudo()
        batch_id = op_batch.search(
            [('name', '=', kw['batch']), ('course_id.name', '=', kw['course'])]).id
        op_subject_model = op_subject.search([('name', '=', kw['subject']), (
            'batch_id', '=', batch_id), ('class_id.name', '=', kw['group']), ('semester_id.name', '=', kw['semester'])])[0]
        all_student = op_student.search(
            [('batch_id', '=', op_subject_model.batch_id.id), ('class_id.name', '=', kw['group'])])
        op_period_id = op_period.search([('name', '=', kw['session'])]).id
        student_lines = []

        for stu_id in all_student:
            roll_number = op_student.browse(stu_id.id).roll_number
            stu_line = [0] * 3
            stu_line[0] = 0
            stu_line[1] = False
            stu_line[2] = {'roll_number': roll_number,
                           'student_id': stu_id.id,
                           'present': True if roll_number in kw['lines'] else False,
                           'course_id': op_subject_model.course_id.id,
                           'class_id': op_subject_model.class_id.id,
                           'batch_id': op_subject_model.batch_id.id,
                           'semester_id': op_subject_model.semester_id.id,
                           'period_id': op_period_id,
                           'subject_id': op_subject_model.id}
            if 'remark' in kw:
                if roll_number in kw['remark']:
                    stu_line[2]['remark'] = kw['remark'][roll_number]

            student_lines.append(stu_line)

        vals = {'attendance_line': student_lines,
                'name': op_subject_model.id,
                'course_id': op_subject_model.course_id.id,
                'class_id': op_subject_model.class_id.id,
                'batch_id': op_subject_model.batch_id.id,
                'semester_id': op_subject_model.semester_id.id,
                'faculty_id': op_subject_model.faculty_id.id,
                'period_id': op_period_id,
                'attendance_date': kw['date']}
        op_attendance.create(vals)
        return "ok"

    @http.route('/create-timetable', type='json', auth='public', cors='*', csrf=False)
    def create_timetable(self, **kw):
        op_course = request.env['op.course'].sudo()
        op_semester = request.env['op.semester'].sudo()
        op_batch = request.env['op.batch'].sudo()
        op_week = request.env['op.week'].sudo()
        op_class = request.env['op.class'].sudo()
        op_subject = request.env['op.subject'].sudo()
        op_faculty = request.env['op.faculty'].sudo()
        op_period = request.env['op.period'].sudo()
        generate_timetable = request.env['generate.time.table'].sudo()
        gtt_line = request.env['gen.time.table.line'].sudo()

        course_id = op_course.search([('name', '=', kw['course'])])
        batch_id = op_batch.search(
            [('name', '=', kw['batch']), ('course_id', '=', course_id.id)])
        class_id = op_class.search(
            [('name', '=', kw['group']), ('batch_id', '=', batch_id.id), ('course_id', '=', course_id.id)])
        semester_id = op_semester.search(
            [('name', '=', kw['semester']),
             ('batch_id', '=', batch_id.id),
             ('class_id', '=', class_id.id),
             ('course_id', '=', course_id.id)])
        week_id = op_week.search([('name', '=', kw['week']),
                                  ('batch_id.name', '=', batch_id.name),
                                  ('semester_id.name', '=', semester_id.name),
                                  ('course_id.name', '=', kw['course']),
                                  ('class_id', '=', class_id.name)])
        check_week_ids = op_week.search(
            [('start_date', '>=', week_id.start_date), ('start_date', '<=', week_id.end_date)])
        check_tt_ids = generate_timetable.search(
            [('week_id', 'in', check_week_ids.ids)])
        tt_ids = generate_timetable.search([('week_id', '=', week_id.id)])
        if tt_ids:
            raise Exception("Timetable already Created")

        def get_gen_time_table_line_data(line, i):
            subject_id = op_subject.search([('name', '=', line['subject']),
                                            ('class_id.name', '=', class_id.name),
                                            ('batch_id.name', '=', batch_id.name),
                                            ('course_id', '=', course_id.id),
                                            ('semester_id.name', '=', semester_id.name)]).id
            faculty_id = op_faculty.search(
                [('name', '=', line['faculty'])]) if 'faculty' in line else False
            period_id = op_period.search([('name', '=', line['session'])])
            if faculty_id:
                for tt_id in check_tt_ids:
                    check_tt_line = gtt_line.search([
                        ('gen_time_table', '=', tt_id.id),
                        ('day', '=', i),
                        ('faculty_id', '=', faculty_id.id),
                        ('period_id', '=', period_id.id)], limit=1)
                    if check_tt_line:
                        raise Exception("Faculty %s already assigned to %s %s \nin %s %s" % (
                            faculty_id.name, tt_id.batch_id.name, tt_id.class_id.name, day_array[i-1], period_id.name))
            res = {'subject_id': subject_id, 'period_id': period_id.id}
            if faculty_id:
                res['faculty_id'] = faculty_id.id
            return res

        val = {
            'semester_id': semester_id.id,
            'is_import': False,
            'class_id': class_id.id,
            'state': 'done',
            'week_id': week_id.id,
            'course_id': course_id.id,
            'start_date': week_id.start_date,
            'end_date': week_id.end_date,
            'batch_id': batch_id.id,
        }
        for i in range(1, 8):
            if kw['lines']['line%s' % i]:
                in_line = []
                for line in kw['lines']['line%s' % i]:
                    data = get_gen_time_table_line_data(line, i)
                    val_line = [0, False, {
                        'subject_id': data['subject_id'],
                        'period_id': data['period_id'],
                        'day': '%s' % i,
                    }]
                    if 'faculty_id' in data:
                        val_line[2]['faculty_id'] = data['faculty_id']
                    in_line.append(val_line)
                val['time_table_lines_%s' % i] = in_line
        generate_timetable.create(val)
        return "ok"

    @http.route('/get-attendance-line', type='http', auth='public', cors='*', csrf=False)
    def get_attendance_line(self, **kw):
        attendance_sheet_obj = request.env['op.attendance.sheet'].sudo()
        attendance_sheets = None
        attendance_sheet_ids = []

        if 'lineNumber' in kw:
            if not 'lastId' in kw:
                request.cr.execute(
                    "select id from op_attendance_sheet order by id desc limit 5")
            else:
                request.cr.execute(
                    "select id from op_attendance_sheet where id < %s order by id desc limit 10" % kw['lastId'])
            attendance_sheet_ids = [i[0] for i in request.cr.fetchall()]
            attendance_sheets = attendance_sheet_obj.browse(
                attendance_sheet_ids)
        lines = []

        for sheet in attendance_sheets:
            for line in sheet.attendance_line:
                lines.append({
                    'student': line.student_id.last_name + ' ' + line.student_id.name or '',
                    'subject': line.subject_id.name or '',
                    'session': line.period_id.name or '',
                    'present': line.present or '',
                    'date': line.attendance_date or '',
                    'absent': line.total_absent_check or '',
                    'classes': line.total_no_of_classes or '',
                    'percentile': line.percentile_of_attendance or '',
                    'batch': line.batch_id.name or '',
                    'group': line.class_id.name or '',

                })
        return json.dumps(lines)

    @http.route('/get-report-data', type='http', auth='public', cors='*', csrf=False)
    def get_report_data(self, **kw):
        attendance_sheet_obj = request.env['op.attendance.sheet'].sudo()
        op_subject = request.env['op.subject'].sudo()
        # gtt = request.env['generate.time.table'].sudo()
        res = {}

        batch = kw['batch']

        domain = [
            ('batch_id.name', '=', kw['batch']),
            ('semester_id.name', '=', kw['semester']),
            ('course_id.name', '=', kw['course']),
            ('class_id.name', '=', kw['group']),
        ] if batch != 'Batch 6' else ['|',
                                      ('batch_id.name', '=', kw['batch']),
                                      ('batch_id.name', '=', 'Batch 5'),
                                      ('semester_id.name',
                                       '=', kw['semester']),
                                      ('course_id.name', '=', kw['course']),
                                      ('class_id.name', '=', kw['group']),
                                      ]
        subject_ids = op_subject.search(domain)
        attendance_domain = [
            ('attendance_date', '>=', kw['startDate']),
            ('attendance_date', '<=', kw['endDate'])
        ] if 'startDate' in kw else []
        tt_domain = [
            ('start_date', '>=', kw['startDate']),
            ('end_date', '<=', kw['endDate'])
        ] if 'startDate' in kw else []
        attendance_domain.extend(domain)
        tt_domain.extend(domain)

        attendance_sheets = attendance_sheet_obj.search(attendance_domain)
        # gtt_ids = gtt.search(tt_domain)

        res['total'] = {
            o.name: 0 for o in subject_ids if "Exam" not in o.name and "Guest" not in o.name}
        res['data'] = {o.name: {}
                       for o in subject_ids if "Exam" not in o.name and "Guest" not in o.name}

        # if gtt_ids:
        #     for gtt_id in gtt_ids:
        #         for line in gtt_id.time_table_lines:
        #             if line.subject_id.name not in res['total']:
        #                 continue
        #             res['total'][line.subject_id.name] += 1

        total_session_attendance = {
            o.name: 0 for o in subject_ids if "Exam" not in o.name and "Guest" not in o.name}

        if attendance_sheets:
            for sheet in attendance_sheets:
                for line in sheet.attendance_line:
                    if not line.present or line.subject_id.name not in res['data']:
                        continue
                    stu_name = line.student_id.last_name + ' ' + line.student_id.name or ''
                    if stu_name in res['data'][line.subject_id.name]:
                        res['data'][line.subject_id.name][stu_name] += 1
                    else:
                        res['data'][line.subject_id.name][stu_name] = 1
                subject_name = sheet.name.name
                if "Exam" in subject_name or "Guest" in subject_name:
                    continue
                total_session_attendance[subject_name] += 1
        res['attendance_session'] = total_session_attendance
        return json.dumps(res)

    @http.route('/get-subject-data', type='http', auth='public', cors='*', csrf=False)
    def get_subject_data(self, **kw):
        op_course = request.env['op.course'].sudo()
        op_batch = request.env['op.batch'].sudo()
        op_class = request.env['op.class'].sudo()
        op_semester = request.env['op.semester'].sudo()
        batch_session = request.env['batch.session'].sudo()
        op_week = request.env['op.week'].sudo()
        op_subject = request.env['op.subject'].sudo()
        all_courses = op_course.search([])
        res = {}

        for course in all_courses:
            all_batches = op_batch.search([('course_id', '=', course.id)])
            for batch in all_batches:
                domain = [('course_id', '=', course.id),
                          ('batch_id', '=', batch.id)]
                all_semester = op_semester.search(domain)
                for semester in all_semester:
                    all_classes = op_class.search(
                        [('course_id', '=', course.id), ('batch_id', '=', batch.id)])
                    for clas in all_classes:
                        domain = [
                            ('course_id.name', '=', course.name),
                            ('batch_id.name', '=', batch.name),
                            ('semester_id.name', '=', semester.name),
                        ]
                        batch_session_ids = batch_session.search(domain)
                        domain.append(('class_id.name', '=', clas.name))
                        subject_ids = op_subject.search(domain)
                        week_ids = op_week.search(domain)
                        assign(res, [course.name, batch.name, semester.name, clas.name, 'subjects'], [
                               {'faculty': o.faculty_id.name, 'subject': o.name} for o in subject_ids])
                        if not 'session' in res[course.name][batch.name][semester.name][clas.name] and batch_session_ids:
                            res[course.name][batch.name][semester.name][clas.name]['session'] = [
                                o.name for o in batch_session_ids.session_line]
                        if not 'week' in res[course.name][batch.name][semester.name][clas.name]:
                            res[course.name][batch.name][semester.name][clas.name]['week'] = [
                                {'name': o.name, 'id': o.id, 'startDate': o.start_date, 'endDate': o.end_date} for o in week_ids]
        return json.dumps(res)

    @http.route('/get-sem-id', type='http', auth='public', cors='*', csrf=False)
    def get_semester_id(self, **kw):
        sem_id = request.env['op.semester'].sudo().search(
            [('start_date', '<=', kw['date']),
             ('end_date', '>=', kw['date']),
             ('batch_id.name', '=', kw['batch']),
             ('name', 'ilike', 'Semester'),
             ('course_id.name', '=', kw['course'])]
        )
        if sem_id:
            return sem_id[0].name
        return ""

    @http.route('/get-user-timetable', type='http', auth='public', cors='*', csrf=False)
    def get_user_timetable(self, **kw):
        uid = request.session.uid or 1
        res_users = request.env['res.users'].sudo()
        groub_obj = request.env['res.groups'].sudo()
        gtt = request.env['generate.time.table'].sudo()
        user = request.env['res.users'].sudo().browse(uid)

        group_bo_ids = groub_obj.search(
            [('name', '=', 'Back Office Admin')]).users.ids
        res = {}
        domain = [
            ('start_date', '<=', kw['date']),
            ('end_date', '>=', kw['date']),
        ]
        tt_ids = gtt.search(domain)
        faculty_name = res_users.search([('id', '=', uid)]).name

        if tt_ids:
            for tt_id in tt_ids:
                if kw['role']  == 'class_monitor' and \
                        (user.x_batch_id.name != tt_id.batch_id.name
                         or user.class_id.name != tt_id.class_id.name):
                    continue
                day_name = datetime.datetime.strptime(
                    kw['date'], '%Y-%m-%d').strftime('%A')
                tt_lines = tt_id['time_table_lines_%d' % day[day_name]]
                temp = {}
                value = {}
                if uid == 1 or uid in group_bo_ids:
                    value = {o.period_id.name: {'subject': o.subject_id.name,
                                                'faculty': o.faculty_id.name} for o in tt_lines}
                elif kw['role'] in ['class_monitor', 'class_adviser'] and user.x_batch_id.name == tt_id.batch_id.name:
                    value = {o.period_id.name: {'subject': o.subject_id.name,
                                                'faculty': o.faculty_id.name} for o in tt_lines}
                else:
                    value = {o.period_id.name: {'subject': o.subject_id.name, 'faculty': o.faculty_id.name}
                             for o in tt_lines if o.faculty_id.name == faculty_name}
                if not value:
                    continue
                if tt_id.course_id.name in res:
                    if tt_id.batch_id.name in res[tt_id.course_id.name]:
                        if tt_id.semester_id.name in res[tt_id.course_id.name][tt_id.batch_id.name]:
                            res[tt_id.course_id.name][tt_id.batch_id.name][tt_id.semester_id.name][tt_id.class_id.name] = value
                        else:
                            assign(temp, [tt_id.class_id.name], value)
                            res[tt_id.course_id.name][tt_id.batch_id.name][tt_id.semester_id.name] = temp
                    else:
                        assign(temp, [tt_id.semester_id.name,
                                      tt_id.class_id.name], value)
                        res[tt_id.course_id.name][tt_id.batch_id.name] = temp
                else:
                    assign(res, [tt_id.course_id.name, tt_id.batch_id.name,
                                 tt_id.semester_id.name, tt_id.class_id.name], value)
        return json.dumps(res)

    @http.route('/get-faculty-timetable', type='json', auth='public', cors='*', csrf=False)
    def get_faculty_timetable(self, **kw):
        uid = request.session.uid or 266
        res_users = request.env['res.users'].sudo()
        gtt = request.env['generate.time.table'].sudo()
        tt_line = request.env['gen.time.table.line'].sudo()
        start_date = kw['date'][0]
        end_date = kw['date'][1]
        # tt_ids = gtt
        tt_ids = gtt.search([
            '|',
            '&', ('start_date', '<=', start_date), ('end_date', '>=', start_date),
            '&', ('start_date', '<=', end_date), ('end_date', '>=', end_date)
        ])
        date_time_obj = datetime.datetime.strptime(start_date, '%m/%d/%Y')
        # for date in kw['date']:
        #     tt_id = gtt.search(
        #         [('start_date', '<=', date), ('end_date', '>=', date)])
        #     for i in tt_id:
        #         if i.id in tt_ids.ids:
        #             continue
        #         tt_ids += i
        res = {}
        val = {}
        for tt_id in tt_ids:
            tt_lines = tt_line.search([('gen_time_table', '=', tt_id.id)])
            date_time_obj = datetime.datetime.strptime(
                tt_id.start_date, '%Y-%m-%d')
            week_day = date_time_obj.weekday() + 1
            for line in tt_lines:
                add_day = int(line.day) - week_day
                if add_day < 0:
                    add_day += 7
                temp_date = date_time_obj + datetime.timedelta(days=add_day)
                if not (
                    temp_date >= datetime.datetime.strptime(
                        start_date, '%m/%d/%Y')
                    and temp_date <= datetime.datetime.strptime(end_date, '%m/%d/%Y')
                ):
                    continue
                if res_users.browse(uid).name != line.faculty_id.name and uid != 1:
                    continue
                val = {
                    'session': line.period_id.name,
                    'faculty': line.faculty_id.name or '',
                    'subject': line.subject_id.name,
                    'batch': tt_id.batch_id.name,
                    'group': tt_id.class_id.name,
                    'semester': tt_id.semester_id.name,
                    'course': tt_id.course_id.name,
                    'week': tt_id.week_id.name
                }
                if line.day in res:
                    res[line.day].append(val)
                else:
                    res[line.day] = [val]
        return json.dumps(res)

    @http.route('/get-student-timetable', type='http', auth='public', cors='*', csrf=False)
    def get_student_timetable(self, **kw):
        res = {}
        tt_id = None
        gtt = request.env['generate.time.table'].sudo()
        tt_line = request.env['gen.time.table.line'].sudo()

        if not 'week' in kw:
            tt_id = gtt.search([('start_date', '<=', kw['date']),
                                ('end_date', '>=', kw['date']),
                                ('batch_id.name', '=', kw['batch']),
                                ('class_id.name', '=', kw['group']),
                                ('course_id.name', '=', kw['course'])])
        else:
            tt_id = gtt.search([('week_id', '=', int(kw['week']))])
        tt_lines = tt_line.search([('gen_time_table', '=', tt_id.id)])
        res['header'] = {
            'course': tt_id.course_id.name,
            'batch': tt_id.batch_id.name,
            'semester': tt_id.semester_id.name,
            'group': tt_id.class_id.name,
            'week': tt_id.week_id.name
        }
        for line in tt_lines:
            if line.faculty_id:
                val = line.subject_id.name + ' ~ ' + line.faculty_id.name
            else:
                val = line.subject_id.name
            if line.period_id.name in res:
                res[line.period_id.name][line.day] = val
            else:
                assign(res, [line.period_id.name, line.day], val)
        return json.dumps(res)

    @http.route('/get-timetable-data', type='http', auth='public', cors='*', csrf=False)
    def get_time_table_data(self, **kw):
        res = {}
        course = kw['course']
        semester = kw['semester']
        batch = kw['batch']
        group = kw['group']
        week = kw['week']
        tt = request.env['generate.time.table'].sudo()
        tt_line = request.env['gen.time.table.line'].sudo()

        tt_id = tt.search([
            ('course_id.name', '=', course),
            ('batch_id.name', '=', batch),
            ('semester_id.name', '=', semester),
            ('class_id.name', '=', group),
            ('week_id.name', 'ilike', week)
        ])
        tt_lines = tt_line.search([('gen_time_table', '=', tt_id.id)])
        for line in tt_lines:
            val = line.subject_id.name + ' ~ ' + \
                line.faculty_id.name if line.faculty_id.name else line.subject_id.name
            if line.period_id.name in res:
                res[line.period_id.name][line.day] = val
            else:
                assign(res, [line.period_id.name, line.day], val)
        return json.dumps({'data': res, 'id': tt_id.id})

    @http.route('/get-timetable-list', type='http', auth='public', cors='*', csrf=False)
    def get_timetable_view(self, **kw):
        course = kw['course']
        batch = kw['batch']
        semester = kw['semester']
        group = kw['group']
        tt_obj = request.env['generate.time.table'].sudo()
        tt_ids = tt_obj.search([('course_id.name', '=', course),
                                ('batch_id.name', '=', batch),
                                ('class_id.name', '=', group),
                                ('semester_id.name', '=', semester)])
        return json.dumps([{'name': o.week_id.name, 'startDate': o.start_date, 'endDate': o.end_date, 'state': o.state, 'id': o.id} for o in tt_ids])

    @http.route('/get-user-profile', type='http', auth='public', cors='*')
    def get_uid(self, **kw):
        uid = request.session.uid or  1
        role = ""
        info = {}
        group_obj = request.env['res.groups'].sudo()
        op_semester = request.env['op.semester'].sudo()
        user = request.env['res.users'].sudo().browse(uid)
        class_monitor_ids = group_obj.search(
            [('name', '=', 'Class Monitor')]).users.ids
        group_student_ids = group_obj.search(
            [('name', '=', 'Student')]).users.ids
        group_class_adviser = group_obj.search(
            [('name', '=', 'Class Adviser')]).users.ids
        group_bo_ids = group_obj.search(
            [('name', '=', 'Back Office Admin')]).users.ids
        if uid == 1 or uid in group_bo_ids:
            role = "admin"
        elif uid in class_monitor_ids:
            role = "class_monitor"
        elif uid in group_class_adviser:
            role = "class_adviser"
        elif uid in group_student_ids:
            role = "student"
            last_name, name = user.name.split(' ')
            student = request.env['op.student'].sudo().search(
                [('name', '=', name), ('last_name', '=', last_name)])
            info = {
                'course': student.course_id.name,
                'batch': student.batch_id.name,
                'group': student.class_id.name,
                'semester': op_semester.search([
                    ('course_id.name', '=', student.course_id.name),
                    ('batch_id.name', '=', student.batch_id.name),
                    ('class_id.name', '=', student.class_id.name),
                    ('start_date', '<=', datetime.datetime.today().date()),
                    ('end_date', '>=', datetime.datetime.today().date()),
                ]).name or 'Semester 1'
            }
        else:
            role = "faculty"

        if role == 'class_adviser' or role == 'class_monitor':
            info = {
                'course': user.course_id.name,
                'batch': user.x_batch_id.name,
                'group': user.class_id.name,
                'semester': op_semester.search([
                    ('course_id.name', '=', user.course_id.name),
                    ('batch_id.name', '=', user.x_batch_id.name),
                    ('class_id.name', '=', user.class_id.name),
                    ('start_date', '<=', datetime.datetime.today().date()),
                    ('end_date', '>=', datetime.datetime.today().date()),
                ]).name or 'Semester 1'
            }
        res = {'role': role, 'name': user.name,
               'info': info}
        return json.dumps(res)

    @http.route('/edit-time-table', type='json', auth='public', cors='*', csrf=False)
    def edit_time_table(self, **kw):
        gtt = request.env['generate.time.table'].sudo()
        tt_line = request.env['gen.time.table.line'].sudo()
        op_faculty = request.env['op.faculty'].sudo()
        op_subject = request.env['op.subject'].sudo()
        op_period = request.env['op.period'].sudo()
        gtt_id = gtt.browse(kw['id'])
        check_ids = gtt.search([('start_date', '>=', gtt_id.week_id.start_date), (
            'start_date', '<=', gtt_id.week_id.end_date), ('id', '!=', gtt_id.id)])
        delete_ids = []
        for session in kw['data']:
            for day in kw['data'][session]:
                val = kw['data'][session][day]
                tt_line_id = tt_line.search(
                    [('gen_time_table', '=', kw['id']), ('day', '=', day), ('period_id.name', '=', session)])
                if val is None:
                    delete_ids.append(tt_line_id.id)
                    continue
                else:
                    split_data = kw['data'][session][day].split(' ~ ')
                    subject = split_data[0]
                    faculty = split_data[1] if len(split_data) == 2 else False
                    faculty_id = op_faculty.search([('name', '=', faculty)])
                    period_id = op_period.search([('name', '=', session)]).id
                    for tt_id in check_ids:
                        if not faculty_id:
                            continue
                        check_tt_line = tt_line.search([
                            ('gen_time_table', '=', tt_id.id),
                            ('faculty_id', '=', faculty_id.id),
                            ('day', '=', day),
                            ('period_id', '=', period_id)], limit=1)
                        if check_tt_line:
                            raise Exception("Faculty %s already assigned to %s %s \nin %s %s" % (
                                faculty_id.name, tt_id.batch_id.name, tt_id.class_id.name, day_array[int(day)-1], session))
                if not tt_line_id:
                    subject_id = op_subject.search([
                        ('name', '=', subject),
                        ('batch_id', '=', gtt_id.batch_id.id),
                        ('course_id', '=', gtt_id.course_id.id),
                        ('semester_id', '=', gtt_id.semester_id.id),
                        ('class_id', '=', gtt_id.class_id.id), ]).id
                    faculty_id = op_faculty.search([('name', '=', faculty)])
                    period_id = op_period.search([('name', '=', session)]).id
                    tt_line.create({
                        'gen_time_table': kw['id'],
                        'faculty_id': faculty_id.id,
                        'subject_id': subject_id,
                        'period_id': period_id,
                        'day': day
                    })
                else:
                    if subject == tt_line_id.faculty_id.name and faculty == tt_line_id.faculty_id.name:
                        continue
                    else:
                        if faculty:
                            faculty_id = op_faculty.search(
                                [('name', '=', faculty)])
                            tt_line_id.write({'faculty_id': faculty_id.id})
                        else:
                            tt_line_id.write({'faculty_id': False})
                        if subject != tt_line_id.subject_id.name:
                            subject_id = op_subject.search([
                                ('name', '=', subject),
                                ('batch_id', '=', gtt_id.batch_id.id),
                                ('course_id', '=', gtt_id.course_id.id),
                                ('semester_id', '=', gtt_id.semester_id.id),
                                ('class_id', '=', gtt_id.class_id.id),
                            ]).id
                            tt_line_id.write({'subject_id': subject_id})
        if delete_ids:
            tt_line.browse(delete_ids).unlink()
        return "ok"

    @http.route('/duplicate-timetable', type='json', auth='public', cors='*', csrf=False)
    def duplicate_time_table(self, **kw):
        gtt = request.env['generate.time.table'].sudo()
        op_week = request.env['op.week'].sudo()
        gtt_line = request.env['gen.time.table.line'].sudo()

        course = kw['course']
        batch = kw['batch']
        semester = kw['semester']
        group = kw['group']
        selected_week = kw['selected']
        week_from = kw['weekFrom']

        def get_gtt_domain(week):
            return [
                ('course_id.name', '=', course),
                ('batch_id.name', '=', batch),
                ('semester_id.name', '=', semester),
                ('class_id.name', '=', group),
                ('week_id.name', '=', week)
            ]

        old_gtt = gtt.search(get_gtt_domain(week_from))

        for week in selected_week:
            if gtt.search(get_gtt_domain(week)):
                raise Exception('Time table for %s is already created' % week)
            new_week = op_week.search([
                ('course_id.name', '=', course),
                ('batch_id.name', '=', batch),
                ('semester_id.name', '=', semester),
                ('class_id.name', '=', group),
                ('name', '=', week)
            ])
            new_week_start_date = datetime.datetime.strptime(
                new_week.start_date, '%Y-%m-%d')
            start_date_search = (new_week_start_date -
                                 timedelta(days=6)).date()
            check_gtt_ids = gtt.search(
                [('start_date', '>=', start_date_search), ('end_date', '<=', new_week.end_date)])
            for tt_line in old_gtt.time_table_lines:
                if not tt_line.faculty_id.id:
                    continue
                for check_id in check_gtt_ids:
                    start_check_day = 1
                    check_start_date = datetime.datetime.strptime(
                        check_id.start_date, '%Y-%m-%d')
                    if new_week_start_date > check_start_date:
                        start_check_day = (
                            new_week_start_date - check_start_date).days
                    elif new_week_start_date < check_start_date:
                        start_check_day = (
                            check_start_date - new_week_start_date).days + 1
                    if int(tt_line.day) > start_check_day:
                        check_tt_line = gtt_line.search([
                            ('gen_time_table', '=', check_id.id),
                            ('day', '=', tt_line.day),
                            ('faculty_id', '=', tt_line.faculty_id.id),
                            ('period_id', '=', tt_line.period_id.id)], limit=1)
                        if check_tt_line:
                            raise Exception("Faculty %s already assigned to %s %s \nin %s %s"
                                            % (tt_line.faculty_id.name, check_id.batch_id.name, check_id.class_id.name, day_array[int(tt_line.day)-1], tt_line.period_id.name))
            old_gtt.copy({'week_id': new_week.id})
        return "ok"

    @http.route('/delete-timetable', type='json', auth='public', cors='*', csrf=False)
    def delete_timetable(self, **kw):
        generate_timetable = request.env['generate.time.table'].sudo()
        gtt_line = request.env['gen.time.table.line'].sudo()
        gen_time_table_obj = generate_timetable.browse(kw['id'])
        gtt_line.search([('gen_time_table', '=', kw['id'])]).unlink()
        gen_time_table_obj.unlink()
        return "ok"
