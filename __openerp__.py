{
    'name': "School Management System",
    'version': "2.0",
    'description': 'School Management System Web Application',
    'author': "Earlyphant",
    'installable': True,
    'data': ['views/webpage.xml', 'views/sms2.xml', 'views/attendance_report_template.xml', 'views/attendance_report.xml'],
    'depends': ['web', 'openeducat_core', 'openeducat_attendance', 'openeducat_facility', 'openeducat_timetable'],
}
