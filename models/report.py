from openerp import models
import time
from openerp.report import report_sxw
import datetime
from dateutil.parser import parse
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT


class AttendanceReport(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        params = context['active_ids'][0].split(' ')
        print ""
        super(AttendanceReport, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_all_student': self.get_all_student,
            'get_subjects': self.get_subjects,
            'attendanceSheetID': int(params[0]),
            'start_date': params[1] or '',
            'end_date': params[2] or '',
            'get_stu_attendance': self.get_stu_attendance,
            'get_attendance_line': self.get_attendance_line,
            'get_header': self.get_header,
            'get_month': self.get_month,
            'get_total_session': self.get_total_session,
            'get_attendance_data': self.get_attendance_data,
            'reportType': 'monthly' if params[1] != 'undefined' else 'semester'
        })
        self.context = context

    def get_stu_attendance(self, attendanceline):
        res = {}
        for line in attendanceline:
            if line['student'] in res:
                if line['subject'] in res[line['student']]:
                    res[line['student']][line['subject']] += 1
                else:
                    res[line['student']][line['subject']] = 1
            else:
                temp = {}
                temp[line['subject']] = 1
                res[line['student']] = temp
        return res

    def get_header(self):
        attendance_sheet_obj = self.pool.get('op.attendance.sheet')
        return attendance_sheet_obj.browse(self.cr, self.uid, self.localcontext['attendanceSheetID'])

    def get_month(self):
        return datetime.datetime.strptime(self.localcontext['start_date'], '%Y-%m-%d').strftime('%B')

    def get_total_session(self):
        attendance_sheet_obj = self.pool.get('op.attendance.sheet')
        header = self.get_header()
        subject_ids = self.get_subjects(self.localcontext['attendanceSheetID'])
        domain = [
            ('batch_id.name', '=', header.batch_id.name),
            ('semester_id.name', '=', header.semester_id.name),
            ('course_id.name', '=', header.course_id.name),
            ('class_id.name', '=', header.class_id.name),
        ]

        attendance_domain = [
            ('attendance_date', '>=', self.localcontext['start_date']),
            ('attendance_date', '<=', self.localcontext['end_date'])
        ] if self.localcontext['start_date'] != 'undefined' else []

        domain.extend(attendance_domain)

        attendance_sheets = attendance_sheet_obj.search(
            self.cr, self.uid, domain)
        res = {o: 0 for o in subject_ids}

        if attendance_sheets:
            for sheet in attendance_sheet_obj.browse(self.cr, self.uid, attendance_sheets):
                subject_name = sheet.name.name
                if "Exam" in subject_name or "Guest" in subject_name or "Intern" in subject_name:
                    continue
                res[subject_name] += 1
        print "res=====================", res
        return res

    def get_attendance_data(self):
        attendance_sheet_obj = self.pool.get('op.attendance.sheet')
        header = self.get_header()
        subject_ids = self.get_subjects(self.localcontext['attendanceSheetID'])

        domain = [
            ('batch_id.name', '=', header.batch_id.name),
            ('semester_id.name', '=', header.semester_id.name),
            ('course_id.name', '=', header.course_id.name),
            ('class_id.name', '=', header.class_id.name),
        ]

        attendance_domain = [
            ('attendance_date', '>=', self.localcontext['start_date']),
            ('attendance_date', '<=', self.localcontext['end_date'])
        ] if self.localcontext['start_date'] != 'undefined' else []

        domain.extend(attendance_domain)

        attendance_sheets = attendance_sheet_obj.search(
            self.cr, self.uid, domain)

        res = {o: {} for o in subject_ids}

        if attendance_sheets:
            for sheet in attendance_sheet_obj.browse(self.cr, self.uid, attendance_sheets):
                for line in sheet.attendance_line:
                    if not line.present or line.subject_id.name not in res:
                        continue
                    stu_name = line.student_id.last_name + ' ' + line.student_id.name or ''
                    if stu_name in res[line.subject_id.name]:
                        res[line.subject_id.name][stu_name] += 1
                    else:
                        res[line.subject_id.name][stu_name] = 1
        return res

    def get_attendance_line(self):
        attendance_sheet_obj = self.pool.get('op.attendance.sheet')
        attendance_sheet_id = attendance_sheet_obj.browse(
            self.cr, self.uid, self.localcontext['attendanceSheetID'])
        attendance_sheet_ids = attendance_sheet_obj.search(self.cr, self.uid, [
            ('attendance_date', '>=', self.localcontext['start_date']),
            ('attendance_date', '<=', self.localcontext['end_date']),
            ('batch_id.name', '=', attendance_sheet_id.batch_id.name),
            ('semester_id.name', '=', attendance_sheet_id.semester_id.name),
            ('course_id.name', '=', attendance_sheet_id.course_id.name),
            ('class_id.name', '=', attendance_sheet_id.class_id.name),
        ])
        attendance_sheets = attendance_sheet_obj.browse(
            self.cr, self.uid, attendance_sheet_ids)
        lines = []
        for sheet in attendance_sheets:
            for line in sheet.attendance_line:
                if not line.present:
                    continue
                lines.append({
                    'student': line.student_id.last_name + ' ' + line.student_id.name or '',
                    'subject': line.subject_id.name or '',
                    'session': line.period_id.name or '',
                    'present': line.present or '',
                    'date': line.attendance_date or '',
                    'absent': line.total_absent_check or '',
                    'classes': line.total_no_of_classes or '',
                    'percentile': line.percentile_of_attendance or '',
                    'batch': line.batch_id.name or '',
                    'group': line.class_id.name or '',

                })
        return lines

    def get_all_student(self, attendance_sheet_id):
        attendance_sheet = self.pool.get('op.attendance.sheet')
        op_student = self.pool.get('op.student')
        cr = self.cr
        uid = self.uid
        attendance_sheet_ids = attendance_sheet.search(
            cr, uid, [('id', '=', attendance_sheet_id)])
        atten_sheet_rec = attendance_sheet.browse(
            cr, uid, attendance_sheet_ids)
        student_ids = op_student.browse(cr, uid, op_student.search(cr, uid, [
            ('batch_id', '=', atten_sheet_rec.batch_id.id),
            ('class_id', '=', atten_sheet_rec.class_id.id)
        ]))
        return student_ids

    def get_subjects(self, attendance_sheet_id):
        cr = self.cr
        uid = self.uid
        attendance_sheet = self.pool.get('op.attendance.sheet')
        op_subject = self.pool.get('op.subject')
        attendance_sheet_ids = attendance_sheet.search(
            cr, uid, [('id', '=', attendance_sheet_id)])
        atten_sheet_rec = attendance_sheet.browse(
            cr, uid, attendance_sheet_ids)
        subject_ids = op_subject.browse(cr, uid, op_subject.search(cr, uid,
                                                                   [('batch_id', '=', atten_sheet_rec.batch_id.id),
                                                                    ('course_id', '=',
                                                                       atten_sheet_rec.course_id.id),
                                                                       ('class_id', '=',
                                                                        atten_sheet_rec.class_id.id),
                                                                       ('semester_id', '=',
                                                                        atten_sheet_rec.semester_id.id),
                                                                       ('name', 'not ilike',
                                                                        'Exam'),
                                                                       ('name', 'not ilike',
                                                                        'Guest'),
                                                                       ('name', 'not ilike',
                                                                        'Internship'),
                                                                    ]))
        return [o.name for o in subject_ids]


class ReportDaily(models.AbstractModel):
    _name = "report.sms2.attendance_report_qweb"
    _inherit = "report.abstract_report"
    _template = "sms2.attendance_report_qweb"
    _wrapped_report_class = AttendanceReport
