from openerp import models, api, _
from openerp import http
import werkzeug.utils
from datetime import datetime


class BottleManufacture(models.TransientModel):
    _name = 'sms2'


    @api.multi
    def open_sms_ui(self):
        uid =  self.env.uid
        url = ""
        group_obj = self.env['res.groups']
        class_monitor_ids = group_obj.search([('name', '=', 'Class Monitor')]).users.ids
        group_student_ids = group_obj.search([('name', '=', 'Student')]).users.ids
        group_class_adviser = group_obj.search([('name', '=', 'Class Adviser')]).users.ids
        if uid == 1:
            url = "/sms/attendance"
        elif uid in class_monitor_ids:
            url = "/sms/attendance"
        elif uid in group_class_adviser:
            url = "/sms/attendance"
        elif uid in group_student_ids:
            url = "/sms/timetable"
        else:
            url = "/sms/attendance"
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self',
        }

    @api.model
    def duplicate_week(self):
        op_course = self.env['op.course']
        op_batch = self.env['op.batch']
        op_class = self.env['op.class']
        op_week = self.env['op.week']
        gtt = self.env['generate.time.table']
        current_date = str(datetime.now().date())
        all_courses = op_course.search([])

        for course in all_courses:
            all_batches = op_batch.search([('course_id', '=', course.id)])
            for batch in all_batches:
                all_classes = op_class.search([('course_id', '=', course.id), ('batch_id', '=', batch.id)])
                for clas in all_classes:
                    tt_id = gtt.search([
                        ('course_id', '=', course.id),
                        ('batch_id', '=', batch.id),
                        ('class_id', '=', clas.id),
                        ('start_date', '<=', current_date),
                        ('end_date', '>=', current_date)
                    ])
                    if not tt_id:
                        self.env.cr.execute('select id from generate_time_table where course_id = %s and batch_id = %s and class_id = %s order by id desc limit 1' %(course.id, batch.id, clas.id))
                        lastest_id = self.env.cr.fetchone()
                        week_id = op_week.search([
                            ('course_id', '=', course.id),
                            ('batch_id', '=', batch.id),
                            ('class_id', '=', clas.id),
                            ('start_date', '<=', current_date),
                            ('end_date', '>=', current_date)
                        ])
                        if lastest_id and week_id:
                            gtt.browse(lastest_id[0]).copy({'week_id':week_id.id})

