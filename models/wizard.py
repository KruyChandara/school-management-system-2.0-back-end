import datetime
from openerp import models, fields, api


class DailyReport(models.TransientModel):
    _name = 'attendance.report'
    report_date = fields.Date('Report Date')

    @api.multi
    def report_daily_details(self):
        data = {
            'ids': self.ids,
            'model': 'hotel.folio',
            'form': self.read(['report_date'])[0]
        }
        return self.env['report'].get_action(self, 'sms2.attendance_report_qweb', data=data)
